import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'firebase';
import { Header, Button, CardSection, Spinner } from './components/common'
import LoginForm from './components/LoginForm'

class App extends Component {
  state = { loggedIn: null }

  componentWillMount() {
    firebase.initializeApp({
      apiKey: "AIzaSyDVupy2uR8CdGOtC6sdHcRTGeWnhaRceNw",
      authDomain: "auth-d2e71.firebaseapp.com",
      databaseURL: "https://auth-d2e71.firebaseio.com",
      projectId: "auth-d2e71",
      storageBucket: "auth-d2e71.appspot.com",
      messagingSenderId: "921492697402"
    });

    firebase.auth().onAuthStateChanged((user) => {
      // Whenever the user signs in/out, this function is called.
      if (user) {
        this.setState({
          loggedIn: true
        })
      } else {
        this.setState({
          loggedIn: false
        })
      }
    })
  }

  renderContent() {
    switch (this.state.loggedIn) {
      case true:
      return (
          <CardSection>
            <Button onPress={() => firebase.auth().signOut()}>
              Log Out
            </Button>
          </CardSection>
        );
      case false:
        return <LoginForm />;
      default:
        return (
          <CardSection>
            <Spinner size="large" />
          </CardSection>
        );
    }
  }

  render() {
    return (
      <View>
        <Header headerText="Authentication" />
        {this.renderContent()}
      </View>
    )
  }
}

export default App;
