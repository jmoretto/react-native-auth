import React, { Component } from 'react';
import { Text } from 'react-native';
import firebase from 'firebase';
import { Button, Card, CardSection, Input, Spinner } from './common';

/*
  Notes:

  TextInputs are like image tags. They have no height/width by default.
*/

class LoginForm extends Component {
  state = {
    email: '',
    password: '',
    error: '',
    loading: false
  };

  async onButtonPress() {
    const { email, password } = this.state;

    // Clear the error.
    this.setState({
      error: '',
      loading: true
    })

    try {
      await firebase.auth().signInWithEmailAndPassword(email, password);
      this.onLoginSuccess()
    } catch (e) {
      // Failed to sign-in, attempt to create an account.
      try {
        await firebase.auth().createUserWithEmailAndPassword(email, password);
        this.onLoginSuccess()
      } catch(e) {
        // Creating user failed, show an error to the user.
        this.onLoginFail()
      }
    }
  }

  onLoginFail() {
    this.setState({
      error: 'Authentication Failed.',
      loading: false
    });
  }

  onLoginSuccess() {
    this.setState({ 
      email: '',
      password: '',
      loading: false,
      error: ''
     })
  }

  renderButton() {
    if (this.state.loading) {
      return <Spinner size="small" />;
    }

    return (
      <Button onPress={this.onButtonPress.bind(this)}>
        Log in
      </Button>
    );
  }

  render() {
    return (
      <Card>
        <CardSection>
          <Input
            value={this.state.email}
            onChangeText={email => this.setState({ email })}
            label='Email'
            placeholder='user@gmail.com'
           />
        </CardSection>

        <CardSection>
          <Input
            value={this.state.password}
            onChangeText={password => this.setState({ password })}
            label='Password'
            placeholder='password'
            secureTextEntry
           />
        </CardSection>

        <Text style={styles.errorTextStyle}>
          {this.state.error}
        </Text>

        <CardSection>
          {this.renderButton()}
        </CardSection>
      </Card>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
}

export default LoginForm;
